<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::resources([
    'getCampaign' => 'CampaignController',
    'getUser' => 'UserController',
    'getPlasma' => 'BplasmaController',
    'getPartner' => 'PartnerController',
    'getContacts' => 'MessageController',
    'getGallery' => 'GalleryController',
    'getGalleryImage' => 'GalleryImageController',
]);
Route::get('/bloodsearch','SearchController@index');
Route::get('/plasma','PlasmaController@index');
Route::get('/registerplasma','RegisterController@index');
Route::post('/plasmaapplyForm', 'PlasmaController@store')->name('apply');
Route::get('/getPlace/{id}','PlasmaController@getPlace')->name('getPlace');
Route::get('/getInfo/{id}','PlasmaController@getInfo')->name('getInfo');
Route::get('/getLocation/{id}','RegisterController@getLocation')->name('getLocation');
Route::get('/contact','FormController@index');
Route::post('/contactform','FormController@store');
Route::get('/getFields/{id}','SearchController@getFields')->name('getFields');
Route::get('/getData/{id}','SearchController@getData')->name('getData');
Route::get('/', 'MainController@index');
Route::get('/event/{id}', 'MainController@show')->name('event.profile');
Route::get('/about-us', 'FrontEnd\MembersController@index');
Route::get('/events','FrontEnd\EventsController@index');
Route::get('/getFields1/{id}','DataController@getFields1')->name('getFields1');
Route::get('/form', function () {
    return view('frontEnd.apply');
});
Route::get('/user/{id}', 'TimesController@apply')->name('getUser.apply');
Route::post('/applyForm1', 'TimesController@applyForm');
Route::get('getGalleryImage/{id}', array('as' => 'show_album', 'uses' => 'GalleryImageController@show'));
Route::get('addimage/{id}', array('as' => 'add_image', 'uses' => 'GalleryImageController@create'));

Route::get('gallery', 'FrontEnd\GalleryController@gallery');

Route::get('gallery/{id}', 'FrontEnd\GalleryController@single_gallery')->name('gsingle');

Auth::routes([
    'register' => true,
    'reset' => false
]);
Route::get('/chart', 'HomeController@index');
Route::get('admin', 'HomeController@index');
Route::post('/applyForm', 'MainController@store')->name('apply');
Route::get('/home', 'HomeController@index')->name('home');
// Route::get('toast', 'HomeController@toast')->name('toast');
