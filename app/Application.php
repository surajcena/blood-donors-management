<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Application extends Model
{
  public function details()
  {
      return $this->belongsTo('App\Details');
  }
}
