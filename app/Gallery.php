<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    protected $fillable = ['name', 'image'];

    public function images(){
        return $this->hasMany(GalleryImage::class);
    }
}
