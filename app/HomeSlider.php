<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HomeSlider extends Model
{

  protected $fillable = ['name', 'description','organizer','venue','date','phone','time', 'image'];

}
