<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Place;
use App\Bloodgroup;
use App\Details;
class DataController extends Controller
{
    public function index(){
      $bloodgroup=Bloodgroup::all();
      return view('frontEnd.index',compact('bloodgroup'));
    }

    public function getFields1($id){
     $places=Place::where('bloodgroup_id',$id)->pluck('name','id');
     return json_encode($places);
    }

    // public function getData($id){
    //   $data=Details::where('place_id',$id)->paginate(10);
    //   return view('frontEnd.search.data_view',compact('data'));
    // }
}
