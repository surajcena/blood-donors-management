<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Vacancy;
use App\Application;
use App\User;
use App\Details;
use Illuminate\Support\Facades\Auth;

class TimesController extends Controller
{
  public function __construct()
  {
      $this->middleware(['auth','verified']);
  }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $user = User::all();
      $user1=Auth::user();
      return view('fileViewer.message.message', compact('user','user1'));
    }

    public function apply($id){
        $item = Details::find($id);
        return view('admin.user.apply', compact('item'));
    }

    public function applyForm(Request $request){
        $app = new Application();
        $app->name = $request->get('name');
        $app->reason = $request->get('reason');
        $details = Details::find($request->get('user_id'));


        if($details->application()->save($app)){
            $request->session()->flash('alert-success', 'Form submitted Successfully');
        } else {
            $request->session()->flash('alert-success', 'Error in submission. try again later');
        }
        return redirect('/getUser');
     }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
