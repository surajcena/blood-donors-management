<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Plasmaplace;
use App\Plasmabloodgroup;
use App\Plasmadetails;
use DB;
use DateTime;
class PlasmaController extends Controller
{
    public function index(){
      $plasmagroup=Plasmabloodgroup::all();
      return view('frontEnd.plasma.plasmasearch',compact('plasmagroup'));
    }

    public function getPlace($id){
     $locations=Plasmaplace::where('plasmabloodgroup_id',$id)->pluck('name','id');
     return json_encode($locations);
    }

    public function getInfo($id){
      $plasmagroups=Plasmabloodgroup::all();
      $info=Plasmadetails::where('plasmaplace_id',$id)->orderBy(DB::raw('RAND()'))->paginate(4);
      return view('frontEnd.plasma.plasma_data_view',compact('info','plasmagroups'));

    }

    public function store(Request $request)
      {

      // $bloodgroup = Bloodgroup::find($request->get('post_id'));
      // $bloodgroup = new Bloodgroup();
      // $bloodgroup->name=$request->bname;
      // $bloodgroup = Bloodgroup::find($request->get('bname'));

     $detail = new Plasmadetails();
     $detail->plasmaplace_id = $request->places;
     $detail->name = $request->input("yname");
     $detail->phone = $request->input("phone");
     $detail->email = $request->input("email");
     $detail->location = $request->input("location");

     if ($detail->save()) {
         toastr()->success('Saved Successfully', 'Successful');
     } else {
         toastr()->error('Error while Entering', 'Problem. Revisit the inputs.');
     }
     return redirect('/');
     }

}
