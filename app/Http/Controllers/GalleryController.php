<?php

namespace App\Http\Controllers;

use App\Gallery;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;

class GalleryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
        $galleries = Gallery::with('images')->get();
        return view('admin.gallery.gallery', compact('galleries'));
    }


    public function create()
    {
        return view('admin.gallery.create');
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'image' => 'image|mimes:jpg,jpeg,png,JPG,JPEG,PNG|max:4096|required',
        ]);

        if ($validator->fails()) {
            toastError('Please correct the problems.', 'Validation Error');
            return redirect('getGallery/create')
                ->withErrors($validator)
                ->withInput();
        }

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $input['imagename'] = 'GalleryAlbum ' . '-' . time() . '.' . $image->getClientOriginalName();

            $location =  ('images/gallery');
            if (!File::exists($location)) {
                File::makeDirectory($location, 0777, true, true);
            }
            $img = Image::make($image->getRealPath());
            $img->resize(640, 560, function ($constraint) {
                $constraint->aspectRatio();
            })->save($location . '/' . $input['imagename']);
        }

        $gallery = Gallery::create([
            'name' => $request->name,
            'image' => $input['imagename']
        ]);
        toastSuccess('Gallery added successfully.', 'Successful');
        return redirect('getGallery');
    }

    public function show($id)
    {
    }

    public function edit($id)
    {
        $gallery = Gallery::findOrFail($id);
        return view('admin.gallery.edit', compact('gallery'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'image' => 'image|mimes:jpg,jpeg,png,JPG,JPEG,PNG|max:4096',
        ],
            $messages = [
                'mimes' => 'Only JPG, JPEG, PNG are allowed.'
            ]);

        $gallery = Gallery::findOrFail($id);
        $input['imagename'] = $gallery->image;
        if ($request->hasFile('image')) {
            $usersImage =  ("images/gallery/{$gallery->image}"); // get previous image from folder
            if (File::exists($usersImage)) { // unlink or remove previous image from folder
                unlink($usersImage);
            }

            $image = $request->file('image');
            $input['imagename'] = 'GalleryAlbum ' . '-' . time() . '.' . $image->getClientOriginalName();

            $location =  ('images/gallery');
            if (!File::exists($location)) {
                File::makeDirectory($location, 0777, true, true);
            }
            $img = Image::make($image->getRealPath());
            $img->resize(640, 560, function ($constraint) {
                $constraint->aspectRatio();
            })->save($location . '/' . $input['imagename']);
        }

        $gallery->update([
            'name' => $request->name,
            'image' => $input['imagename']
        ]);

        toastSuccess("Updated Successfully", 'Successful');
        return redirect('getGallery');
    }

    public function destroy($id)
    {
        $gallery = Gallery::findOrFail($id);
        $filename = '/images/gallery/' . $gallery->image;
        File::delete($filename);
        $gallery->images()->delete();
        if ($gallery->delete()) {
            toastSuccess('Deleted Successfully', 'Successful');
        } else {
            toastError('Problem while deleting', 'Error');
        }
        return redirect('getGallery');
    }
}
