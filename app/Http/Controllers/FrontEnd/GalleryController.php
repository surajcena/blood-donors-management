<?php

namespace App\Http\Controllers\FrontEnd;

use App\Gallery;
use App\Http\Controllers\Controller;


class GalleryController extends Controller
{
    public function gallery()
    {

        $galleries = Gallery::with('images')->get();
        return view('frontEnd.gallery.gallery', compact('galleries'
            ));
    }

    public function single_gallery($id)
    {


        $gallery = Gallery::findOrFail($id);
        return view('frontEnd.gallery.single-gallery', compact('gallery'));
    }
}
