<?php

namespace App\Http\Controllers;

use App\Partner;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;

class PartnerController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $partners = Partner::all();
        return view('admin.partner.partner', compact('partners'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.partner.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'image' => 'image|mimes:jpg,jpeg,png,JPG,JPEG,PNG|max:2048|required',
        ]);

        if ($validator->fails()) {
            toastError('Please submit valid image.', 'Image Validation Error');
            return redirect('getPartner/create')
                ->withErrors($validator)
                ->withInput();
        }

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $input['imagename'] = 'Partner ' . '-' . time() . '.' . $image->getClientOriginalName();

            $location =  ('images/partners');
            if (!File::exists($location)) {
                File::makeDirectory($location, 0777, true, true);
            }
            $img = Image::make($image->getRealPath());
            $img->save($location . '/' . $input['imagename']);
        }

        $partner = Partner::create([
            'name' => $request->name,
            'link' => $request->link,
            'image' => $input['imagename'],
        ]);

        toastSuccess('Successfully added.', 'Successful');
        return redirect('getPartner');
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Partner $partner
     * @return \Illuminate\Http\Response
     */
    public function show(Partner $partner)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Partner $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $partner = Partner::findOrFail($id);
        return view('admin.partner.edit', compact('partner'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Partner $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'image' => 'image|mimes:jpg,jpeg,png,JPG,JPEG,PNG|max:2048',
        ],
            $messages = [
                'mimes' => 'Only JPG, JPEG, PNG, DOC, DOCX, PDF are allowed.'
            ]);

        $partner = Partner::findOrFail($id);
        $input['imagename'] = $partner->image;
        if ($request->hasFile('image')) {
            $usersImage = ("images/partners/{$partner->image}"); // get previous image from folder
            if (File::exists($usersImage)) { // unlink or remove previous image from folder
                unlink($usersImage);
            }

            $image = $request->file('image');
            $input['imagename'] = 'Partner ' . '-' . time() . '.' . $image->getClientOriginalName();

            $location =  ('images/partners');
            if (!File::exists($location)) {
                File::makeDirectory($location, 0777, true, true);
            }
            $img = Image::make($image->getRealPath());
            $img->save($location . '/' . $input['imagename']);
        }

        $partner->update([
            'name' => $request->name,
            'link' => $request->link,
            'image' => $input['imagename'],
        ]);

        toastSuccess("Updated Successfully", 'Successful');
        return redirect('getPartner');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Partner $partner
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $partner = Partner::findOrFail($id);
        $filename =  '/images/partners/' . $partner->image;
        File::delete($filename);
        if ($partner->delete()) {
            toastSuccess('Deleted Successfully', 'Successful');
        } else {
            toastError('Problem while deleting', 'Error');
        }
        return redirect('getPartner');
    }
}
