<?php

namespace App\Http\Controllers;

use App\Form;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\File;

class FormController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function index()
    {
        return view('frontEnd.contact.contact');
    }



    public function store(Request $request)
    {
        $form = new Form();
        $form->name = $request->user_name;
        $form->subject = $request->user_subject;
        $form->email = $request->user_email;
        $form->phone = $request->user_phone;
        $form->description = $request->user_desctiption;

        if ($form->save()) {
            toastr()->success('Saved Successfully', 'Successful');
        } else {
            toastr()->error('Error while Entering', 'Problem. Revisit the inputs.');
        }
        return redirect('contact');
    }


}
