<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Plasmaplace;
use App\Plasmabloodgroup;
use App\Details;
class RegisterController extends Controller
{
    public function index(){
      $plasmagroup=Plasmabloodgroup::all();
      $place=Plasmaplace::all();
      return view('frontEnd.plasma.register',compact('plasmagroup','place'));
    }

    public function getLocation($id){
     $places=Plasmaplace::where('plasmabloodgroup_id',$id)->pluck('name','id');
     return json_encode($places);
    }

    // public function getData($id){
    //   $data=Details::where('place_id',$id)->paginate(10);
    //   return view('frontEnd.search.data_view',compact('data'));
    // }
}
