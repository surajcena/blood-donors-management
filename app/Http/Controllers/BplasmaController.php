<?php

namespace App\Http\Controllers;

use App\HomeSlider;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;
use App\Plasmadetails;
use App\Plasmabloodgroup;
use App\Plasmaplace;
use DB;

class BplasmaController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $users=Plasmadetails::all();
        $places=Plasmaplace::all();
        $bloodgroups=Plasmabloodgroup::all();
        // $application=Application::all();
    //     $application1=Application::orderBy('created_at', 'desc')->take(1)->get();
    //     foreach ($application1 as $author) {
    //     foreach($users as $user){
    //       if($author->details_id==$user->id)
    //       echo $author->name;
    //     }
    //
    // }
         // dd($application1);
        return view('admin.Plasma.plasma', compact('users','bloodgroups','places'));
    }


    public function edit($id)
    {
        $detail = Plasmadetails::findOrFail($id);
        return view('admin.plasma.edit', compact('detail'));
    }

    public function update(Request $request, $id)
    {

        $details = Plasmadetails::findOrFail($id);
        $details->name = $request->get('name');
        $details->phone = $request->get('phone');

        if ($details->save()) {
            toastr()->success('Updated Successfully', 'Successful');
        } else {
            toastr()->error('Problem while updating', 'Error.');
        }
        return redirect('/getPlasma');
    }

    public function destroy($id)
    {
        $user = Plasmadetails::findOrFail($id);

        if ($user->delete()) {
            toastr()->success('Deleted Successfully', 'Successful');
        } else {
            toastr()->error('Problem while deleting', 'Error');
        }
        return redirect('getPlasma');
    }

}
