<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// use App\Course;
// use App\Curricular;
use App\Form;
use App\Partner;
use App\Gallery;
use App\HomeSlider;
use App\Details;
use App\Plasmadetails;
// use App\Post;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
      $sliders = HomeSlider::all();
      // $notice = Post::where('type', 'notice')->get();
      // $news = Post::where('type', 'news')->get();
      // $events = Post::where('type', 'events')->get();
      // $results = Post::where('type', 'results')->get();
      $details = Details::all();
      $galleries = Gallery::all();
      $partners = Partner::all();
      $messages = Form::all();
      //
      $plasma = Plasmadetails::all();
       $userData = Details::select(\DB::raw("COUNT(*) as count"))
                    ->whereYear('created_at', date('Y'))
                    ->groupBy(\DB::raw("Month(created_at)"))
                    ->pluck('count');

      // $district = District::all();
      // $ilist = IList::all();
      // $constituent = IList::where('type', 'Constituent')->get();
      // $affiliated = IList::where('type', 'Affiliated')->get();
      // $tecs = IList::where('type', 'TECS')->get();
      // $imap = IMap::all();

      return view('admin.index', compact('sliders','details','galleries','plasma','messages','partners','userData'
          // 'notice', 'news', 'events', 'results', 'faculties',
          // 'galleries', 'curricular', 'download', 'course',
          // 'district', 'ilist', 'constituent', 'affiliated',
          ));
    }
   
}
