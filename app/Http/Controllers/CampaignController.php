<?php

namespace App\Http\Controllers;

use App\HomeSlider;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;

class CampaignController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $sliders = HomeSlider::orderBy('created_at', 'desc')->get();
        return view('admin.home-slider.homeslider', compact('sliders'));
    }

    public function create()
    {
        return view('admin.home-slider.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'image' => 'image|mimes:jpeg,png,PNG,JPEG |max:4096|required',
        ],
            $messages = [
                'mimes' => 'Only jpeg, png, PNG, JPEG are allowed.'
            ]);

        $slider = new HomeSlider();
        $slider->name = $request->name;
        $slider->description = $request->description;
        $slider->organizer = $request->oname;
        $slider->venue = $request->vname;
        $slider->date = $request->date;
        $slider->phone = $request->phone;
        $slider->time = $request->time;

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $input['imagename'] = 'sliders ' . '-' . time() . '-' . $image->getClientOriginalName();

            $location = ('images/sliders');
            if (!File::exists($location)) {
                File::makeDirectory($location, 0777, true, true);
            }
            $img = Image::make($image->getRealPath());
//            $img->orientate();
            $img->resize(300, 380, function ($constraint) {
                $constraint->aspectRatio();
            })->save($location . '/' . $input['imagename']);

            $slider->image = $input['imagename'];
        }

        if ($slider->save()) {
            toastr()->success('Saved Successfully', 'Successful');
        } else {
            toastr()->error('Error while deleting', 'Problem. Revisit the inputs.');
        }
        return redirect('getCampaign');
    }

    public function edit($id)
    {
        $slider = HomeSlider::findOrFail($id);
        return view('admin.home-slider.edit', compact('slider'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'image' => 'image|mimes:jpeg,png,JPEG,PNG |max:4096',
        ],
            $messages = [
                'mimes' => 'Only jpeg, png, JPEG, PNG are allowed.'
            ]);

        $sliders = HomeSlider::findOrFail($id);
        $sliders->name = $request->get('name');
        $sliders->description = $request->get('description');
        $sliders->organizer = $request->get('oname');
        $sliders->venue = $request->get('vname');
        $sliders->date = $request->get('date');
        $sliders->phone = $request->get('phone');
        $sliders->time = $request->get('time');

        if ($request->hasFile('image')) {
            $usersImage = ("images/sliders/{$sliders->image}"); // get previous image from folder
            if (File::exists($usersImage)) { // unlink or remove previous image from folder
                unlink($usersImage);
            }
            $image = $request->file('image');
            $input['imagename'] = 'sliders ' . '-' . time() . '.' . $image->getClientOriginalName();

            $location =  ('images/sliders');
            if (!File::exists($location)) {
                File::makeDirectory($location, 0777, true, true);
            }
            $img = Image::make($image->getRealPath());
            $img->orientate();
            $img->resize(300, 380, function ($constraint) {
                $constraint->aspectRatio();
            })->save($location . '/' . $input['imagename']);

            $sliders->image = $input['imagename'];
        }

        if ($sliders->save()) {
            toastr()->success('Updated Successfully', 'Successful');
        } else {
            toastr()->error('Problem while updating', 'Error.');
        }
        return redirect('getCampaign');
    }

    public function destroy($id)
    {
        $sliders = HomeSlider::findOrFail($id);
        $filename =  '/images/sliders/' . $sliders->image;
        File::delete($filename);
        if ($sliders->delete()) {
            toastr()->success('Deleted Successfully', 'Successful');
        } else {
            toastr()->error('Problem while deleting', 'Error');
        }
        return redirect('getCampaign');
    }
}
