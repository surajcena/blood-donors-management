<?php

namespace App\Http\Controllers;

use App\HomeSlider;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;
use App\Details;
use App\Bloodgroup;
use App\Place;
use App\Application;
use DB;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $users=Details::all();
        $places=Place::all();
        $bloodgroups=Bloodgroup::all();
        $application=Application::all();
    //     $application1=Application::orderBy('created_at', 'desc')->take(1)->get();
    //     foreach ($application1 as $author) {
    //     foreach($users as $user){
    //       if($author->details_id==$user->id)
    //       echo $author->name;
    //     }
    //
    // }
         // dd($application1);
        return view('admin.user.user', compact('users','bloodgroups','places','application'));
    }


    public function edit($id)
    {
        $detail = Details::findOrFail($id);
        return view('admin.user.edit', compact('detail'));
    }

    public function update(Request $request, $id)
    {

        $details = Details::findOrFail($id);
        $details->Name = $request->get('name');
        $details->Phone = $request->get('phone');

        if ($details->save()) {
            toastr()->success('Updated Successfully', 'Successful');
        } else {
            toastr()->error('Problem while updating', 'Error.');
        }
        return redirect('getUser');
    }

    public function destroy($id)
    {
        $user = Details::findOrFail($id);
        
        if ($user->delete()) {
            toastr()->success('Deleted Successfully', 'Successful');
        } else {
            toastr()->error('Problem while deleting', 'Error');
        }
        return redirect('getUser');
    }

}
