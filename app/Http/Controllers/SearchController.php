<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Place;
use App\Bloodgroup;
use App\Details;
use DB;
use DateTime;
use Carbon\Carbon;
class SearchController extends Controller
{
    public function index(){
      $bloodgroup=Bloodgroup::all();
      return view('frontEnd.search.multisearch',compact('bloodgroup'));
    }

    public function getFields($id){
     $places=Place::where('bloodgroup_id',$id)->pluck('name','id');
     return json_encode($places);
    }

    public function getData($id){
      $bloodgroups=Bloodgroup::all();
      $data=Details::where('place_id',$id)->orderBy(DB::raw('RAND()'))->paginate(4);
      return view('frontEnd.search.data_view',compact('data','bloodgroups'));
    }
}
