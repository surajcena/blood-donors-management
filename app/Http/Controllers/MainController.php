<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Bloodgroup;
use App\Place;
use App\Details;
use App\HomeSlider;
use App\Partner;

class MainController extends Controller
{

    /**
     * Show the application dashboard.
     *
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $member = Members::orderBy('order', 'ASC')->get();
        // $project = Projects::all();
        // $blog = Blogs::all();
        // $news = News::all();
        $partners = Partner::all();
        $sliders = HomeSlider::orderBy('created_at', 'desc')->take('2')->get();
        $bloodgroup=Bloodgroup::all();
        $place=Place::all();
        return view('frontEnd.index',compact('bloodgroup','place','sliders','partners'));
    }

public function store(Request $request)
  {

  // $bloodgroup = Bloodgroup::find($request->get('post_id'));
  // $bloodgroup = new Bloodgroup();
  // $bloodgroup->name=$request->bname;
  // $bloodgroup = Bloodgroup::find($request->get('bname'));

 $detail = new Details();
 $detail->place_id = $request->places;
 $detail->name = $request->input("yname");
 $detail->phone = $request->input("phone");
 $detail->email = $request->input("email");
 $detail->location = $request->input("location");

 if ($detail->save()) {
     toastr()->success('Saved Successfully', 'Successful');
 } else {
     toastr()->error('Error while Entering', 'Problem. Revisit the inputs.');
 }
 return redirect('/');
 }


 public function show($id)
 {

     $item = HomeSlider::find($id);
     return view('frontEnd/events/single-event', compact('item'));
 }
}
