<?php

namespace App\Http\Controllers;

use App\Gallery;
use App\GalleryImage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;

class GalleryImageController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create($id)
    {
        $gallery = Gallery::find($id);
        return view('admin.gallery.createGalleryImages')
            ->with('gallery', $gallery);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'image' => 'image|mimes:jpg,jpeg,png,JPG,JPEG,PNG|max:4096|required',
        ]);

        if ($validator->fails()) {
            toastError('Please correct the problems.', 'Validation Error');
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $item = new GalleryImage();
        $item->name = $request->name;
        $item->gallery_id = $request->gallery_id;
        $gallery = Gallery::find($request->get('gallery_id'));

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $input['imagename'] = 'Gallery ' . '-' . time() . '.' . $image->getClientOriginalName();

            $location = ('images/gallery/');
            if (!File::exists($location)) {
                File::makeDirectory($location, 0777, true, true);
            }
            $img = Image::make($image->getRealPath());
            $img->resize(1200, 800, function ($constraint) {
                $constraint->aspectRatio();
            })->save($location . '/' . $input['imagename']);

            $item->image  = $input['imagename'];
        }

        if($gallery->images()->save($item)){
            toastSuccess('Gallery Image added successfully.', 'Successful');
        } else {
            toastError('Error in uploading.', 'Problem');
        }
        return redirect()->route('show_album',['id'=>$gallery->id]);
    }

    public function show($id)
    {
        $gallery = Gallery::findOrFail($id);
        return view('admin.gallery.single-gallery', compact('gallery'));
    }

    public function destroy($id)
    {
        $image = GalleryImage::find($id);
        $image->delete();
        return redirect()->back();
    }
}
