<?php

namespace App\Http\Controllers;

use App\Form;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\File;

class MessageController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
     public function __construct()
     {
         $this->middleware('auth');
     }

     public function index()
     {
         $messages = Form::orderBy('created_at', 'desc')->get();
         return view('admin.contact.contact', compact('messages'));
     }
     public function destroy($id)
     {
         $messages = Form::findOrFail($id);


         if ($messages->delete()) {
             toastr()->success('Deleted Successfully', 'Successful');
         } else {
             toastr()->error('Problem while deleting', 'Error');
         }
         return redirect('getContacts');
     }
   }
