<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Plasmaplace extends Model
{
  public function plasmabloodgroup(){
    return $this->belongsTo('App\Plasmabloodgroup');
  }
  public function plasmadetails(){
    return $this->hasMany('App\Plasmadetails');
  }
}
