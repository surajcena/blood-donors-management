<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GalleryImage extends Model
{
    protected $fillable = ['name', 'image'];

    public function gallery()
    {
        return $this->belongsTo('App\Gallery');
    }
}
