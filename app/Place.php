<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Place extends Model
{
  public function bloodgroup(){
    return $this->belongsTo('App\Bloodgroup');
  }
  public function details(){
    return $this->hasMany('App\Details');
  }
}
