<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Details extends Model
{
  public function place(){
    return $this->belongsTo('App\Place');
  }
  public function application()
  {
      return $this->hasMany(Application::class);
  }
}
