<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Plasmabloodgroup extends Model
{
  public function plasmaplaces(){
    return $this->hasMany('App\Plasmaplace');
  }
}
