<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Plasmadetails extends Model
{
  public function plasmaplace(){
    return $this->belongsTo('App\Plasmaplace');
  }
}
