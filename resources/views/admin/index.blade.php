@extends('admin.layouts.master')

@section('contents')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Dashboard</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
                            <li class="breadcrumb-item active">Dashboard</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <!-- Info boxes -->
                <div class="row">

                    <div class="col-12 col-sm-6 col-md-3">
                        <div class="info-box">
                            <span class="info-box-icon bg-info elevation-1">
                                <i class="fas fa-photo-video"></i></span>
                            <div class="info-box-content">
                                <span class="info-box-text">Blood Donation Campaign</span>
                                <span class="info-box-number">{{ count($sliders) }}</span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </div>
                    <!-- /.col -->
                    <div class="col-12 col-sm-6 col-md-3">
                        <div class="info-box mb-3">
                            <span class="info-box-icon bg-danger elevation-1">
                                <i class="fas fa-book-medical"></i></span>
                            <div class="info-box-content">
                                <span class="info-box-text">Blood Donors</span>
                                <span class="info-box-number">{{ count($details) }}</span>

                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </div>
                    <!-- /.col -->
                    <!-- fix for small devices only -->
                   <div class="clearfix hidden-md-up"></div>

                   <div class="col-12 col-sm-6 col-md-3">
                       <div class="info-box mb-3">
                           <span class="info-box-icon bg-success elevation-1"><i
                                   class="fas fa-images"></i></span>
                           <div class="info-box-content">
                               <span class="info-box-text">Gallery Albums</span>
                               <span class="info-box-number">{{ count($galleries) }}</span>
                           </div>
                           <!-- /.info-box-content -->
                       </div>
                       <!-- /.info-box -->
                   </div>
                   <!-- /.col -->
                   <div class="col-12 col-sm-6 col-md-3">
                        <div class="info-box mb-3">
                            <span class="info-box-icon bg-gradient-danger elevation-1">
                                <i class="fas fa-handshake"></i></span>
                            <div class="info-box-content">
                                <span class="info-box-text">Partners</span>
                                <span class="info-box-number">{{ count($partners) }}</span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </div>
                    <!-- /.col -->

                    <div class="col-12 col-sm-6 col-md-3">
                         <div class="info-box mb-3">
                             <span class="info-box-icon bg-gradient-warning elevation-1">
                                 <i class="fas fa-envelope"></i></span>
                             <div class="info-box-content">
                                 <span class="info-box-text">User Messages</span>
                                 <span class="info-box-number">{{ count($messages) }}</span>
                             </div>
                             <!-- /.info-box-content -->
                         </div>
                         <!-- /.info-box -->
                     </div>
                     <!-- /.col -->
                     <div class="col-12 col-sm-6 col-md-3">
                          <div class="info-box mb-3">
                              <span class="info-box-icon bg-gradient-primary elevation-1">
                                  <i class="fas fa-hospital-user"></i></span>
                              <div class="info-box-content">
                                  <span class="info-box-text">Plasma Donors</span>
                                  <span class="info-box-number">{{ count($plasma) }}</span>
                              </div>
                              <!-- /.info-box-content -->
                          </div>
                          <!-- /.info-box -->
                      </div>
                      <!-- /.col -->



                    <!-- fix for small devices only -->
                    <div class="clearfix hidden-md-up"></div>
                </div>
                <!-- /.row -->
            </div>
            
    <div id="container"></div>
        </section>
    </div>


    <!-- /.content-wrapper -->
    <script src="https://code.highcharts.com/highcharts.js"></script>

<script type="text/javascript">
    var userData = <?php echo json_encode($userData)?>;

    Highcharts.chart('container', {
        title: {
            text: 'New User Growth'
        },
        subtitle: {
            text: 'Blood Donation'
        },
        xAxis: {
            categories: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September',
                'October', 'November', 'December'
            ]
        },
        yAxis: {
            title: {
                text: 'Number of New Donors'
            }
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle'
        },
        plotOptions: {
            series: {
                allowPointSelect: true
            }
        },
        series: [{
            name: 'New Users',
            data: userData
        }],
        responsive: {
            rules: [{
                condition: {
                    maxWidth: 500
                },
                chartOptions: {
                    legend: {
                        layout: 'horizontal',
                        align: 'center',
                        verticalAlign: 'bottom'
                    }
                }
            }]
        }
    });

</script>

@endsection
