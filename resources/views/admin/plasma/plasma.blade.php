@extends('admin.layouts.master')

@section('css')
    <style>
        .view-page {
            font-size: 60%;
        }
        .view-page:hover{
            text-decoration: underline;
        }
    </style>
@endsection

@section('contents')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Plasma Donors
                            <a href="/" class="view-page">(View Page)</a>
                        </h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ url('admin') }}">Home</a></li>
                            <li class="breadcrumb-item">Plasma Donors</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title float-left">Plasma Donors Table</h3>

                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <table id="table" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>S.N.</th>
                                        <th>Name</th>
                                        <th>Phone</th>
                                        <th>Bloodgroup</th>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($users as $user)
                                        <tr>
                                            <td>{{$user->id}}</td>
                                            <td>{{ $user->name }}</td>
                                            <td>{{ $user->phone }}</td>


                                            @foreach($places as $place )
                                            @foreach($bloodgroups as $bloodgroup)
                                            @if($bloodgroup->id==$place->plasmabloodgroup_id)
                                            @if($place->id==$user->plasmaplace_id)
                                            <td>{{ $bloodgroup->name }}</td>

                                            @endif
                                            @endif
                                            @endforeach
                                            @endforeach


                                            <td>
                                                <button type="button" class="btn btn-primary" title="Edit"
                                                        onclick="location.href='{{route('getPlasma.edit',$user->id)}}';">
                                                    <i class="fas fa-edit"></i></button>
                                                <button type="button" class="btn btn-warning" title="View"
                                                        data-toggle="modal" data-target="#viewModal{{$user->id}}">
                                                    <i class="fas fa-eye"></i></button>
                                                <button type="submit" class="btn btn-danger" title="Remove"
                                                        data-toggle="modal" data-target="#delModal{{$user->id}}">
                                                    <i class="fas fa-trash"></i></button>
                                            </td>
                                        </tr>

                                        <!-- View Modal -->
                                        <div class="modal fade" id="viewModal{{$user->id}}" role="dialog">
                                            <div class="modal-dialog">

                                                <!-- Modal content-->
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h4 class="modal-title">
                                                            Donors {{ $user->name }}</h4>
                                                    </div>
                                                    <div class="modal-body">

                                                        <p>Name: {{ $user->name }}</p>
                                                        <p>Phone: {{ $user->phone }}</p>
                                                        <p>Location :{{$user->plasmaplace->name }}</p>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button class="btn btn-danger btn-default pull-left"
                                                                data-dismiss="modal">Close
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Delete Modal -->
                                        <div class="modal fade" id="delModal{{$user->id}}" role="dialog">
                                            <div class="modal-dialog">

                                                <!-- Modal content-->
                                                <div class="modal-content">
                                                    <div class="modal-header">

                                                        <h4 class="modal-title">Plasma Donor Deletion </h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <p>Are you sure to delete the plasma donor
                                                            '<strong>{{$user->id}} {{ $user->name }}</strong>' ?</p>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button class="btn btn-danger btn-default pull-left"
                                                                data-dismiss="modal">Close
                                                        </button>
                                                        <form action="{{route('getPlasma.destroy', $user->id)}}"
                                                              method="POST">
                                                            {{method_field('DELETE')}}
                                                            {{ csrf_field() }}
                                                            <button type="submit" class="btn btn-danger">Delete</button>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th>S.N.</th>
                                        <th>Name</th>
                                        <th>Phone</th>
                                        <th>Bloodgroup</th>
                                        <th>Actions</th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                            <!-- /.card-body -->

                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.content -->
        <!-- /.content -->
    </div>

    <!-- /.content-wrapper -->

@endsection
