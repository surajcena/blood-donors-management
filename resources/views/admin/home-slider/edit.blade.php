@extends('admin.layouts.master')

@section('contents')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Edit Campaign Details</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ url('admin') }}">Home</a></li>
                            <li class="breadcrumb-item"><a href="{{ url('getCampaign') }}">Campaign</a></li>
                            <li class="breadcrumb-item active">Create</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <!-- left column -->
                    <div class="col-md-12">
                        <!-- Horizontal Form -->
                        <div class="card card-info">
                            <div class="card-header">
                                <h3 class="card-title">Donation Campaign Form</h3>
                            </div>
                            <!-- /.card-header -->
                            <!-- form start -->
                            <form class="form-horizontal" method="POST"
                                  action="{{route('getCampaign.update', $slider->id)}}" method="post"
                                  enctype="multipart/form-data" class="form-horizontal">
                                {{ csrf_field() }}
                                {{method_field('PUT')}}
                                <div class="card-body">
                                    <small>
                                        <ul>
                                            {!! implode('', $errors->all('<li style="color:red">:message</li>')) !!}
                                        </ul>
                                    </small>
                                    <div class="form-group row">
                                        <label for="name" class="col-sm-2 col-form-label">Name</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="name" name="name"
                                                   value="{{ $slider->name }}"
                                                   placeholder="Name">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="description" class="col-sm-2 col-form-label">Description</label>
                                        <div class="col-sm-10">
                                            <textarea type="text" class="form-control" id="description"
                                                      name="description" placeholder="Description">
                                                {!! $slider->description !!}</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="oname" class="col-sm-2 col-form-label">Organizer Name</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="oname" name="oname"
                                                   value="{{ $slider->organizer }}"
                                                   placeholder="Organizer Name">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="vname" class="col-sm-2 col-form-label"> Venue Name</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="vname" name="vname"
                                                   value="{{ $slider->venue }}"
                                                   placeholder="Venue Name">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="date" class="col-sm-2 col-form-label">Date</label>
                                        <div class="col-sm-10">
                                            <input type="date" class="form-control" id="date" name="date"
                                                   value="{{ $slider->date }}"
                                                   placeholder="Date">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="phone" class="col-sm-2 col-form-label">Phone </label>
                                        <div class="col-sm-10">
                                            <input type="tel" class="form-control" id="phone" name="phone"
                                                   placeholder="9812345678" pattern="[9]{1}[0-9]{9}"
                                                   value="{{$slider->phone}}" required>
                                            <small class="form-text text-muted">
                                                Format: 9812345678</small>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="phone" class="col-sm-2 col-form-label">Phone </label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="time" name="time"
                                                   placeholder="3PM -5PM"
                                                   value="{{$slider->time}}" required>
                                            <small class="form-text text-muted">
                                                Format: 3PM -5PM</small>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="image" class="col-sm-2 col-form-label">Image</label>
                                        <div class=" col-sm-10">
                                            <img src="{{url('images/sliders')}}/{{$slider->image}}" height="80px"
                                                 alt="Slider Image"
                                                 title="Image Slider">
                                            <div class="input-group">
                                                <div class="custom-file">
                                                    <input type="file" class="custom-file-input" id="image"
                                                           name="image">
                                                    <label class="custom-file-label" for="image">Choose file</label>
                                                </div>
                                                <div class="input-group-append">
                                                    <span class="input-group-text" id="">Upload</span>
                                                </div>
                                            </div>

                                            <ul>{!! implode('', $errors->all('<li style="color:red">:message</li>')) !!}</ul>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.card-body -->
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-info">Submit</button>
                                    <button type="reset" class="btn btn-default float-right">Reset</button>
                                    <button type="button" class="btn btn-default float-right" title="Edit"
                                            onclick="location.href='{{url()->previous()}}'">
                                        Cancel
                                    </button>
                                </div>
                                <!-- /.card-footer -->
                            </form>
                        </div>
                        <!-- /.card -->

                    </div>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>

    <!-- /.content-wrapper -->

@endsection
@section('js')
    <script>
        $(function () {
            $('#description').summernote()
        })
    </script>
    <script type="application/javascript">
        $('input[type="file"]').change(function(e){
            var fileName = e.target.files[0].name;
            $('.custom-file-label').html(fileName);
        });
    </script>
@endsection
