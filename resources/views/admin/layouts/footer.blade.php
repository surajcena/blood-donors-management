<!-- Main Footer -->
<footer class="main-footer">
    <!-- To the right -->
    <div class="float-right d-none d-sm-inline">
        v.{{env('APP_VERSION')}}
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2020 <a href="#">Blood Database</a>.</strong> All
    rights reserved.
</footer>
