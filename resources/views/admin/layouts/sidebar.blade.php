<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="/" class="brand-link">
        <img src="{{asset('bower_components/admin-lte/dist/img/AdminLTELogo.png')}}" alt="AdminLTE Logo"
             class="brand-image img-circle elevation-3"
             style="opacity: .8">
        <span class="brand-text font-weight-light">Blood Database</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="{{asset('images/admin.png')}}"
                     class="img-circle elevation-2"
                     alt="User Image">
            </div>
            <div class="info">
                <a href="#" class="d-block">  {{auth()->user()->name}}</a>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                     with font-awesome or any other icon font library -->
                <li class="nav-item">
                    <a href="{{ url('admin') }}"
                       class="nav-link {{ Request::url() == url('admin') ? 'active' : '' }}">
                        <i class="nav-icon fas fa-home"></i>
                        Dashboard
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ url('getCampaign') }}"
                       class="nav-link {{ (request()->segment(1) == 'getCampaign') ? 'active' : '' }}">
                        <i class="nav-icon fas fa-photo-video"></i>
                        Donation Campaign
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ url('getUser') }}"
                       class="nav-link {{ request()->is('getUser*') ? 'active' : '' }}">
                        <i class="nav-icon fas fa-user"></i>
                        Users
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ url('getGallery') }}"
                       class="nav-link {{ request()->is('getGallery*') ? 'active' : '' }}">
                        <i class="nav-icon fas fa-image"></i>
                        Gallery
                    </a>
                </li>
                 <li class="nav-item">
                    <a href="{{ url('getPartner') }}"
                       class="nav-link {{ request()->is('getPartner*') ? 'active' : '' }}">
                        <i class="nav-icon fas fa-handshake"></i>
                        Partners
                    </a>
                </li>
                <li class="nav-item">
                   <a href="{{ url('getContacts') }}"
                      class="nav-link {{ request()->is('getContacts*') ? 'active' : '' }}">
                       <i class="nav-icon fas fa-envelope"></i>
                       User Messages
                   </a>
               </li>
               <li class="nav-item">
                  <a href="{{ url('getPlasma') }}"
                     class="nav-link {{ request()->is('getPlasma*') ? 'active' : '' }}">
                      <i class="nav-icon fas fa-hospital-user"></i>
                      Plasma Donors
                  </a>
              </li>

            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
