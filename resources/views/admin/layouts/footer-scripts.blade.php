<!-- jQuery -->
<script src={{ asset("bower_components/admin-lte/plugins/jquery/jquery.min.js") }}></script>
@jquery
@toastr_js
@toastr_render
<!-- Bootstrap 4 -->
<script src={{ asset("bower_components/admin-lte/plugins/bootstrap/js/bootstrap.bundle.min.js") }}></script>
<!-- AdminLTE App -->
<script src={{ asset("bower_components/admin-lte/dist/js/adminlte.min.js") }}></script>

<script src={{asset("bower_components/admin-lte/plugins/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.js")}}></script>
<!-- DataTables -->
<script src={{ asset("bower_components/admin-lte/plugins/datatables/jquery.dataTables.min.js") }}></script>
<script src={{ asset("bower_components/admin-lte/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js") }}></script>
<script src={{ asset("bower_components/admin-lte/plugins/datatables-responsive/js/dataTables.responsive.min.js") }}></script>
<script src={{ asset("bower_components/admin-lte/plugins/datatables-responsive/js/responsive.bootstrap4.min.js") }}></script>
<script src={{ asset("bower_components/admin-lte/plugins/summernote/summernote-bs4.min.js") }}></script>
<script>
    $(function () {
        var t = $('#table').DataTable({
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "responsive": true,
            "columnDefs": [ {
                "searchable": false,
                "orderable": false,
                "targets": 0
            } ],
            "order": [[ 1, 'asc' ]]
        });

        t.on( 'order.dt search.dt', function () {
            t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            });
        }).draw();
    } );

</script>

@yield('js')
