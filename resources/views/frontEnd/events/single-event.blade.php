@extends('frontEnd.layout.master')
@section('body')
<section class="page-header" data-stellar-background-ratio="1.2">

    <div class="container">

        <div class="row">

            <div class="col-sm-12 text-center">


                <h3>
                    {{$item->name}}
                </h3>

                <p class="page-breadcrumb">
                    <a href="/">Home</a> / <a href="/events">Events</a> / {{$item->name}}
                </p>


            </div>

        </div> <!-- end .row  -->

    </div> <!-- end .container  -->

</section> <!-- end .page-header  -->

<section class="section-content-block">

    <div class="container">

        <div class="row">



            <div class="article-event clearfix">

                <div class="col-sm-12">

                    <article class="post single-post-inner">

                        <div class="post-inner-featured-content">
                            <img alt="" src="{{asset('images/sliders')}}/{{$item->image}}">
                        </div>


                        <div class="single-post-inner-title">
                            <h2>{{$item->name}}</h2>
                            <p class="single-post-meta"><i class="fa fa-user"></i>&nbsp; Admin &nbsp; &nbsp; <i class="fa fa-share"></i>&nbsp; Blood, Save Life</p>
                        </div>

                        <div class="single-post-inner-content">
                            {!!$item->description!!}

                        </div>


                    </article> <!--  end single-post-container -->


                </div> <!--  end .single-post-container -->

                <div class="col-sm-4">

                    <h4 class="event-content-title">Details</h4>

                    <p class="event-content-info">

                        <span class="event-sub-content-title">Date:  <em class="date">{{$item->date}}</em></span>
                        <span class="event-sub-content-title">Time:</span>{{$item->time}}


                    </p>
                </div> <!-- end .col-sm-4  -->

                <div class="col-sm-4">

                    <h4 class="event-content-title">Organizer</h4>

                    <p class="event-content-info">
                        {{$item->organizer}} <br />
                        <span class="event-sub-content-title">Phone:</span>
                        {{$item->phone}}  <br />
                    </p>

                </div> <!-- end .col-sm-4  -->

                <div class="col-sm-4">

                    <h4 class="event-content-title">Venue</h4>

                    <p class="event-content-info">
                        {{$item->venue}}
                    </p>


                </div> <!-- end .col-sm-4  -->

            </div>



        </div> <!--  end .row  -->




    </div> <!--  end container -->

</section> <!-- end .section-content-block  -->

@endsection
