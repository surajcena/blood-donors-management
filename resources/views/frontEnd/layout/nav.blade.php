<div class="navbar-collapse collapse">
    <ul class="nav navbar-nav navbar-right">
      <li class={{ Request::url() == url('/') ? 'current-page-item' : '' }}>
            <a href="{{ url('/') }}">home</a>
        </li>




        <!-- <li>
            <a href="#">Campaign</a>
            <ul class="drop-down">
                <li><a href="events.html">All Campaigns</a></li>
                <li><a href="event-single.html">Single Campaign</a></li>
            </ul>
        </li> -->

        <!-- <li class="drop"><a href="#">Pages</a>
            <ul class="drop-down">


                <li><a href="faq.html" title="FAQ">FAQ</a></li>
                <li class="drop"><a href="#">Gallery</a>
                    <ul class="drop-down level3">
                        <li><a href="gallery-1.html">Layout 01</a></li>
                        <li><a href="gallery-2.html">Layout 02</a></li>

                    </ul>
                </li>
                <li><a href="404.html" title="404 Page">404 Page</a></li>
                <li class="drop"><a href="#">Level 3</a>
                    <ul class="drop-down level3">
                        <li><a href="#">Level 3.1</a></li>
                        <li><a href="#">Level 3.2</a></li>
                        <li><a href="#">Level 3.3</a></li>
                    </ul>
                </li>
            </ul>
        </li> -->

        <!-- <li>
            <a href="#">Blog</a>
            <ul class="drop-down">
                <li><a href="blog.html">All Posts</a></li>
                <li><a href="single.html">Single Page</a></li>
            </ul>
        </li> -->
        <li class={{ Request::url() == url('/gallery') ? 'current-page-item' : '' }}>
              <a href="{{ url('/gallery') }}">Gallery</a>
          </li>
        <li class={{ Request::url() == url('/bloodsearch') ? 'current-page-item' : '' }}>
              <a href="{{ url('/bloodsearch') }}">Search Blood</a>
          </li>
          <li class={{ Request::url() == url('/plasma') ? 'current-page-item' : '' }}>
                <a>Plasma</a>
                <ul class="drop-down level3">
                    <li><a href="{{ url('/registerplasma') }}">Register Plasma</a></li>
                    <li><a href="{{ url('/plasma') }}">Search Plasma</a></li>

                </ul>
            </li>
          <li class={{ Request::url() == url('/events') ? 'current-page-item' : '' }}>
                <a href="{{ url('/events') }}">Events</a>
            </li>
        <li class={{ Request::url() == url('/about-us') ? 'current-page-item' : '' }}>
                  <a href="{{ url('/about-us') }}">about us</a>
              </li>

        <li class={{ Request::url() == url('/contact') ? 'current-page-item' : '' }}>
              <a href="{{ url('/contact') }}">Contact</a>
          </li>
    </ul>
</div>
