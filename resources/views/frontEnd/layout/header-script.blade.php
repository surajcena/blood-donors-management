<meta charset="utf-8">
<title>RaktaCode</title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="description" content="Raktacode">
<meta name="author" content="xenioushk">
<link rel="shortcut icon" href="images/favicon.png" />

<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

<!-- The styles -->
<link rel="stylesheet" href="{{asset('frontEnd/css/bootstrap.min.css')}}" />
<link href="{{asset('frontEnd/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css" >
<link href="{{asset('frontEnd/css/animate.css')}}" rel="stylesheet" type="text/css" >
<link href="{{asset('frontEnd/css/owl.carousel.css')}}" rel="stylesheet" type="text/css" >
<link href="{{asset('frontEnd/css/venobox.css')}}" rel="stylesheet" type="text/css" >
<link rel="stylesheet" href="{{asset('frontEnd/css/styles.css')}}" />
<!-- new css -->



    <link rel='stylesheet' href={{ asset('vendors/goodlayers-core/plugins/combine/style.css')}} type='text/css'
          media='all'/>
    <link rel='stylesheet' href={{ asset('vendors/goodlayers-core/include/css/page-builder.css')}} type='text/css'
          media='all'/>
    <!-- <link rel='stylesheet' href={{ asset('vendors/revslider/public/assets/css/settings.css')}} type='text/css'
          media='all'/> -->
    <link rel='stylesheet' href={{ asset('css/style-core.css')}} type='text/css' media='all'/>
    <link rel='stylesheet' href={{ asset('css/kingster-style-custom.css')}} type='text/css' media='all'/>
