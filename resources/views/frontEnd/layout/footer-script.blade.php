<script src="{{asset('frontEnd/js/jquery.min.js')}}"></script>
<script src="{{asset('frontEnd/js/bootstrap.min.js')}}"></script>
<script src="{{asset('frontEnd/js/wow.min.js')}}"></script>
<script src="{{asset('frontEnd/js/jquery.backTop.min.js')}}"></script>
<script src="{{asset('frontEnd/js/waypoints.min.js')}}"></script>
<script src="{{asset('frontEnd/js/waypoints-sticky.min.js')}}"></script>
<script src="{{asset('frontEnd/js/owl.carousel.min.js')}}"></script>
<script src="{{asset('frontEnd/js/jquery.stellar.min.js')}}"></script>
<script src="{{asset('frontEnd/js/jquery.counterup.min.js')}}"></script>
<script src="{{asset('frontEnd/js/venobox.min.js')}}"></script>
<script src="{{asset('frontEnd/js/custom-scripts.js')}}"></script>
<!-- new js -->

<script type='text/javascript'
        src={{asset('vendors/revslider/public/assets/js/jquery.themepunch.tools.min.js')}}></script>
<script type='text/javascript'
        src={{asset('vendors/revslider/public/assets/js/jquery.themepunch.revolution.min.js')}}></script>
<script type="text/javascript"
        src={{asset("vendors/revslider/public/assets/js/extensions/revolution.extension.slideanims.min.js")}}></script>
<script type="text/javascript"
        src={{asset("vendors/revslider/public/assets/js/extensions/revolution.extension.layeranimation.min.js")}}></script>
<script type="text/javascript"
        src={{asset("vendors/revslider/public/assets/js/extensions/revolution.extension.kenburn.min.js")}}></script>
<script type="text/javascript"
        src={{asset("vendors/revslider/public/assets/js/extensions/revolution.extension.navigation.min.js")}}></script>
<script type="text/javascript"
        src={{asset("vendors/revslider/public/assets/js/extensions/revolution.extension.parallax.min.js")}}></script>
<script type="text/javascript"
        src={{asset("vendors/revslider/public/assets/js/extensions/revolution.extension.actions.min.js")}}></script>
<script type="text/javascript"
        src={{asset("vendors/revslider/public/assets/js/extensions/revolution.extension.video.min.js")}}></script>
@yield('js')


<script>
    (function (body) {
        'use strict';
        body.className = body.className.replace(/\btribe-no-js\b/, 'tribe-js');
    })(document.body);
</script>

</script>
<script type="text/javascript">
    /*<![CDATA[*/
    function revslider_showDoubleJqueryError(sliderID) {
        var errorMessage = "Revolution Slider Error: You have some jquery.js library include that comes after the revolution files js include.";
        errorMessage += "<br> This includes make eliminates the revolution slider libraries, and make it not work.";
        errorMessage += "<br><br> To fix it you can:<br>&nbsp;&nbsp;&nbsp; 1. In the Slider Settings -> Troubleshooting set option:  <strong><b>Put JS Includes To Body</b></strong> option to true.";
        errorMessage += "<br>&nbsp;&nbsp;&nbsp; 2. Find the double jquery.js include and remove it.";
        errorMessage = "<span style='font-size:16px;color:#BC0C06;'>" + errorMessage + "</span>";
        jQuery(sliderID).show().html(errorMessage);
    } /*]]>*/
</script>
<script type='text/javascript' src={{ asset('vendors/goodlayers-core/plugins/combine/script.js') }}></script>
<script type='text/javascript'>
    var gdlr_core_pbf = {
        "admin": "",
        "video": {
            "width": "640",
            "height": "360"
        },
        "ajax_url": "https:\/\/demo.goodlayers.com\/kingster\/wp-admin\/admin-ajax.php"
    };
</script>
<script type='text/javascript' src={{ asset('vendors/goodlayers-core/include/js/page-builder.js') }}></script>
<script type='text/javascript' src={{ asset('js/jquery/ui/effect.min.js') }}></script>
<script type='text/javascript'>
    var kingster_script_core = {
        "home_url": "https:\/\/demo.goodlayers.com\/kingster\/"
    };
</script>
<script type='text/javascript' src={{ asset('js/plugins.min.js') }}></script>
