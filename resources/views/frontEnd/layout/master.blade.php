<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->


<!-- Mirrored from templates.bwlthemes.com/blood_donation/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 16 Aug 2020 07:23:32 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=iso-8859-1" /><!-- /Added by HTTrack -->
<head>
  @include('frontEnd.layout.header-script')

    <body>

        <div id="preloader">
            <span class="margin-bottom"><img src="{{asset('frontEnd/images/loader.gif')}}" alt="" /></span>
        </div>

        <!--  HEADER -->

      @include('frontEnd.layout.header')

        <!--  HOME SLIDER BLOCK  -->

      

        <!-- HIGHLIGHT CTA  -->
       @yield('body')

        <!-- START FOOTER  -->

        @include('frontEnd.layout.footer')


        <!-- END FOOTER  -->

        <!-- Back To Top Button  -->

        <a id="backTop">Back To Top</a>

       @include('frontEnd.layout.footer-script')
    </body>


<!-- Mirrored from templates.bwlthemes.com/blood_donation/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 16 Aug 2020 07:23:56 GMT -->
</html>
