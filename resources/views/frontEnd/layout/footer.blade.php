<footer>

    <section class="footer-widget-area footer-widget-area-bg">

        <div class="container">

            <div class="row">

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                    <div class="about-footer">

                        <div class="row">

                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                <img src="{{asset('frontEnd/images/logo4.png')}}" alt="" />
                            </div> <!--  end col-lg-3-->

                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                <p>
                                    We are world largest and trustful blood donation center. We have been working since 1973 with a prestigious vision to helping patient to provide blood.
                                    We are working all over the world, organizing blood donation campaign to grow awareness among the people to donate blood.
                                </p>
                            </div> <!--  end .col-lg-9  -->

                        </div> <!--  end .row -->

                    </div> <!--  end .about-footer  -->

                </div> <!--  end .col-md-12  -->

            </div> <!--  end .row  -->

            <div class="row">

                <div class="col-md-4 col-sm-6 col-xs-12">

                    <div class="footer-widget">
                        <div class="sidebar-widget-wrapper">
                            <div class="footer-widget-header clearfix">
                                <h3>Subscribe Us</h3>
                            </div>
                            <p>Signup for regular newsletter and stay up to date with our latest news.</p>
                            <div class="footer-subscription">
                                <p>
                                    <input id="mc4wp_email" class="form-control" required="" placeholder="Enter Your Email" name="EMAIL" type="email">
                                </p>
                                <p>
                                    <input class="btn btn-default" value="Subscribe Now" type="submit">
                                </p>
                            </div>
                        </div>
                    </div>

                </div> <!--  end .col-md-4 col-sm-12 -->

                <div class="col-md-4 col-sm-6 col-xs-12">

                    <div class="footer-widget">

                        <div class="sidebar-widget-wrapper">

                            <div class="footer-widget-header clearfix">
                                <h3>Contact Us</h3>
                            </div>  <!--  end .footer-widget-header -->


                            <div class="textwidget">

                                <i class="fa fa-envelope-o fa-contact"></i> <p><a href="#">support@donation.com</a><br/><a href="#">helpme@donation.com</a></p>

                                <i class="fa fa-location-arrow fa-contact"></i> <p>location<br/>someplace, Nepal</p>

                                <i class="fa fa-phone fa-contact"></i> <p>Office:&nbsp; (+977) 222 222 222<br/>Cell:&nbsp; (+977) 061 161 </p>

                            </div>

                        </div> <!-- end .footer-widget-wrapper  -->

                    </div> <!--  end .footer-widget  -->

                </div> <!--  end .col-md-4 col-sm-12 -->

                <div class="col-md-4 col-sm-12 col-xs-12">

                    <div class="footer-widget clearfix">

                        <div class="sidebar-widget-wrapper">

                            <div class="footer-widget-header clearfix">
                                <h3>Support Links</h3>
                            </div>  <!--  end .footer-widget-header -->


                            <ul class="footer-useful-links">

                                <li>
                                    <a href="#">
                                        <i class="fa fa-caret-right fa-footer"></i>
                                        Some Links
                                    </a>
                                </li>

                                <li>
                                    <a href="#">
                                        <i class="fa fa-caret-right fa-footer"></i>
                                        Some Links
                                    </a>
                                </li>

                                <li>
                                    <a href="#">
                                        <i class="fa fa-caret-right fa-footer"></i>
                                        Some Links
                                    </a>
                                </li>

                                <li>
                                    <a href="#">
                                        <i class="fa fa-caret-right fa-footer"></i>
                                        Some Links
                                    </a>
                                </li>

                                <li>
                                    <a href="#">
                                        <i class="fa fa-caret-right fa-footer"></i>
                                        Some Links
                                    </a>
                                </li>

                                <li>
                                    <a href="#">
                                        <i class="fa fa-caret-right fa-footer"></i>
                                        Some Links
                                    </a>
                                </li>

                                <li>
                                    <a href="#">
                                        <i class="fa fa-caret-right fa-footer"></i>
                                        Some Links
                                    </a>
                                </li>

                                <li>
                                    <a href="#">
                                        <i class="fa fa-caret-right fa-footer"></i>
                                        Some Links
                                    </a>
                                </li>

                                <li>
                                    <a href="#">
                                        <i class="fa fa-caret-right fa-footer"></i>
                                        Some Links
                                    </a>
                                </li>

                                <li>
                                    <a href="#">
                                        <i class="fa fa-caret-right fa-footer"></i>
                                        Some Links
                                    </a>
                                </li>

                            </ul>

                        </div> <!--  end .footer-widget  -->

                    </div> <!--  end .footer-widget  -->

                </div> <!--  end .col-md-4 col-sm-12 -->

            </div> <!-- end row  -->

        </div> <!-- end .container  -->

    </section> <!--  end .footer-widget-area  -->

    <!--FOOTER CONTENT  -->

    <section class="footer-contents">

        <div class="container">

            <div class="row clearfix">

                <div class="col-md-6 col-sm-12">
                    <p class="copyright-text"> Copyright © 2017, All Right Reserved - by suraj timilsina </p>

                </div>  <!-- end .col-sm-6  -->

                <div class="col-md-6 col-sm-12 text-right">
                    <div class="footer-nav">
                        <nav>
                            <ul>
                              <li class={{ Request::url() == url('/') ? 'current-page-item' : '' }}>
                                    <a href="{{ url('/about-us') }}">Home</a>
                                </li>
                                <li class={{ Request::url() == url('/about-us') ? 'current-page-item' : '' }}>
                                      <a href="{{ url('/about-us') }}">About Us</a>
                                  </li>
                                  <li class={{ Request::url() == url('/bloodsearch') ? 'current-page-item' : '' }}>
                                        <a href="{{ url('/bloodsearch') }}">Search Blood</a>
                                    </li>
                                    <li class={{ Request::url() == url('/gallery') ? 'current-page-item' : '' }}>
                                          <a href="{{ url('/gallery') }}">Gallery</a>
                                      </li>
                                      <li class={{ Request::url() == url('/contact') ? 'current-page-item' : '' }}>
                                            <a href="{{ url('/contact') }}">Contact</a>
                                        </li>

                            </ul>
                        </nav>
                    </div> <!--  end .footer-nav  -->
                </div> <!--  end .col-lg-6  -->

            </div> <!-- end .row  -->

        </div> <!--  end .container  -->

    </section> <!--  end .footer-content  -->

</footer>
