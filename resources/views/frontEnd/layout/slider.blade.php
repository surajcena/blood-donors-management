<div class="slider-wrap">

    <div id="slider_1" class="owl-carousel owl-theme">

        <div class="item">
            <img src="frontEnd/images/home_1_slider_1.jpg" alt="img">
            <div class="slider-content text-center">
                <div class="container">

                    <div class="slider-contents-info">
                        <h3>Donate blood,save life !</h3>
                        <h2>
                            Your Donation Can bring
                            <br>
                            smile at others face
                        </h2>
                        <a href="/bloodsearch" class="btn btn-slider">Search Blood <i class="fa fa-long-arrow-right"></i></a>
                    </div> <!-- end .slider-contents-info  -->
                </div><!-- /.slider-content -->
            </div>
        </div>

        <div class="item">
            <img src="frontEnd/images/home_1_slider_1.jpg" alt="img">
            <div class="slider-content text-center">
                <div class="container">
                    <div class="slider-contents-info">
                        <h3>Donate blood,save life !</h3>
                        <h2>
                            Donate your blood and
                            <br>
                            Inspires to others
                        </h2>
                        <a href="/bloodsearch" class="btn btn-slider">Search Blood <i class="fa fa-long-arrow-right"></i></a>
                    </div><!-- /.slider-content -->
                </div> <!-- end .slider-contents-info  -->
            </div>

        </div>
    </div>

</div>
