@extends('frontEnd.layout.master')
@section('body')
  @toastr_css
<script src="{{ asset('jquery.min.js') }}"></script>
        @include('frontEnd.layout.slider')
        <section class="cta-section-1">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                        <h2>We are helping people from 40 years</h2>
                        <p>
                            You can give blood at any of our blood donation venues all over the world.
                            We have total sixty thousands donor centers and visit thousands of other venues on various occasions.
                        </p>
                    </div> <!--  end .col-md-8  -->
                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                        <a class="btn btn-cta-1" href="#donation">Donate Blood</a>
                    </div> <!--  end .col-md-4  -->
                </div> <!--  end .row  -->
            </div>
        </section>

        <!--  SECTION DONATION PROCESS -->

        <section class="section-content-block section-process">

            <div class="container-fluid">

                <div class="row">

                    <div class="col-md-12 col-sm-12 text-center">
                        <h2 class="section-heading"><span>Donation</span> Process</h2>
                        <p class="section-subheading">The donation process from the time you arrive center until the time you leave</p>
                    </div> <!-- end .col-sm-10  -->

                </div> <!--  end .row  -->

                <div class="row wow fadeInUp">

                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">

                        <div class="process-layout">

                            <figure class="process-img">

                                <img src="frontEnd/images/process_1.jpg" alt="process" />
                                <div class="step">
                                    <h3>1</h3>
                                </div>
                            </figure> <!-- end .process-img  -->

                            <article class="process-info">
                                <h2>Registration</h2>
                                <p>You need to complete a very simple registration form. Which contains all required contact information to enter in the donation process.</p>
                            </article>

                        </div> <!--  end .process-layout -->

                    </div> <!--  end .col-lg-3 -->



                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">

                        <div class="process-layout">

                            <figure class="process-img">
                                <img src="frontEnd/images/process_2.jpg" alt="process" />
                                <div class="step">
                                    <h3>2</h3>
                                </div>
                            </figure> <!-- end .cau<h5 class="step">1</h5>se-img  -->

                            <article class="process-info">
                                <h2>Screening</h2>
                                <p>A drop of blood from your finger will take for simple test to ensure that your blood iron levels are proper enough for donation process.</p>
                            </article>

                        </div> <!--  end .process-layout -->

                    </div> <!--  end .col-lg-3 -->


                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">

                        <div class="process-layout">

                            <figure class="process-img">
                                <img src="frontEnd/images/process_3.jpg" alt="process" />
                                <div class="step">
                                    <h3>3</h3>
                                </div>
                            </figure> <!-- end .process-img  -->

                            <article class="process-info">
                                <h2>Donation</h2>
                                <p>After ensuring and passed screening test successfully you will be directed to a donor bed for donation. It will take only 6-10 minutes.</p>
                            </article>

                        </div> <!--  end .process-layout -->

                    </div> <!--  end .col-lg-3 -->



                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">

                        <div class="process-layout">

                            <figure class="process-img">
                                <img src="frontEnd/images/process_4.jpg" alt="process" />
                                <div class="step">
                                    <h3>4</h3>
                                </div>
                            </figure> <!-- end .process-img  -->

                            <article class="process-info">
                                <h2>Refreshment</h2>
                                <p>You can also stay in sitting room until you feel strong enough to leave our center. You will receive awesome drink from us in donation zone. </p>
                            </article>

                        </div> <!--  end .process-layout -->

                    </div> <!--  end .col-lg-3 -->

                </div> <!--  end .row -->

            </div> <!--  end .container  -->

        </section> <!--  end .section-process -->

        <!--  SECTION COUNTER   -->

        <section class="section-counter"  data-stellar-background-ratio="0.3">

            <div class="container wow fadeInUp">

                <div class="row">

                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">

                        <div class="counter-block-1 text-center">

                            <i class="fa fa-heartbeat icon"></i>
                            <span class="counter">2578</span>
                            <h4>Success Smile</h4>

                        </div>

                    </div> <!--  end .col-lg-3  -->

                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">

                        <div class="counter-block-1 text-center">

                            <i class="fa fa-stethoscope icon"></i>
                            <span class="counter">3235</span>
                            <h4>Happy Donors</h4>

                        </div>

                    </div> <!--  end .col-lg-3  -->

                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">

                        <div class="counter-block-1 text-center">

                            <i class="fa fa-users icon"></i>
                            <span class="counter">3568</span>
                            <h4>Happy Recipient</h4>

                        </div>

                    </div> <!--  end .col-lg-3  -->

                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">

                        <div class="counter-block-1 text-center">

                            <i class="fa fa-building icon"></i>
                            <span class="counter">1364</span>
                            <h4>Total Awards</h4>

                        </div>

                    </div> <!--  end .col-lg-3  -->


                </div> <!-- end row  -->

            </div> <!--  end .container  -->

        </section> <!--  end .section-counter -->

        <!--  SECTION APPOINTMENT   -->

        <section class="section-appointment" id="donation">

            <div class="container wow fadeInUp">

                <div class="row">

                    <div class="col-lg-6 col-md-6 hidden-sm hidden-xs">

                        <figure class="appointment">
                            <img src="frontEnd/images/appointment.jpg" alt="appointment image">
                        </figure>
                    </div>
                    <!--  end col-lg-6  -->


                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">

                        <div class="appointment-form-wrapper text-center clearfix">
                            <h3 class="join-heading">I want to be a donor</h3>
                            <form class="appoinment-form" action="/applyForm" method="post" enctype="multipart/form-data">
                              {{ csrf_field() }}
                              <div class="form-group col-md-6">
                                  <div class="select-style">
                                      <select class="form-control" name="bname" id="bname">
                                        <option selected="false">
                                          Blood Group
                                        </option>
                                        @foreach($bloodgroup as $bg)
                                          <option value="{{$bg->id}}">{{$bg->name}}
                                          </option>
                                          @endforeach
                                      </select>
                                  </div>
                              </div>
                              <div class="form-group col-md-6">
                                  <div class="select-style">
                                      <select class="form-control" name="places" id="places">
                                          @foreach($place as $pe)
                                          <option value="{{$pe->id}}" selected="false" >District...
                                          </option>
                                          @endforeach

                                      </select>
                                  </div>
                              </div>
                                <div class="form-group col-md-6">
                                    <input id="yname" name="yname" class="form-control" placeholder="Name" type="text" required>
                                </div>
                                <div class="form-group col-md-6">
                                    <input id="phone" name="phone" class="form-control" placeholder="Phone" type="text" required>
                                </div>
                                <div class="form-group col-md-6">
                                    <input id="email" name="email" class="form-control" placeholder="Email" type="text"required>
                                </div>
                                <div class="form-group col-md-6">
                                    <input id="location" name="location" class="form-control" placeholder=" Current Location" type="text" required>
                                </div>


                                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                    <button id="btn_submit" class="btn-submit" type="submit">Register</button>
                                </div>

                            </form>

                        </div> <!-- end .appointment-form-wrapper  -->

                    </div> <!--  end .col-lg-6 -->

                </div> <!--  end .row  -->

            </div> <!--  end .container -->

        </section>  <!--  end .appointment-section  -->

        <!-- SECTION TESTIMONIAL   -->

        <section class="section-content-block section-client-testimonial">

            <div class="container">

                <div class="testimonial-container text-center">

                    <div class="col-md-10 col-md-offset-1 col-sm-12">

                        <div class="testimony-layout-1">
                            <h3 class="people-quote">Donor Opinion</h3>
                            <img src="frontEnd/images/team_9.jpg" alt="process" class="img-thumbnail center" style="display: block; border-radius: 50%; margin-left: auto; margin-right: auto; width: 20%;" />
                            <p class="testimony-text">
                                <i class="fa fa-quote-left" aria-hidden="true"></i>
                                I proudly donate blood on a regular basis because it gives others something they desperately need to survive. Just knowing I can make a difference in someone else’s life makes me feel great!
                                <i class="fa fa-quote-right" aria-hidden="true"></i>
                            </p>

                            <h6>Name</h6>
                            <span>Working Field</span>

                        </div> <!-- end .testimony-layout-1  -->

                    </div> <!--  end col-md-10  -->

                    <div class="col-md-10 col-md-offset-1 col-sm-12">

                        <div class="testimony-layout-1">
                            <h3 class="people-quote">Donor Opinion</h3>
                              <img src="frontEnd/images/team_9.jpg" alt="process" class="img-thumbnail center" style="display: block; border-radius: 50%; margin-left: auto; margin-right: auto; width: 20%;" />
                            <p class="testimony-text">
                                <i class="fa fa-quote-left" aria-hidden="true"></i>
                                I have been a donor since high school. Although I have not been a donor every year, I always want to give to the human race. I love to help others! Moreover it gives a real peace in my mind.
                                <i class="fa fa-quote-right" aria-hidden="true"></i>
                            </p>

                            <h6>Name</h6>
                            <span>Working Field</span>

                        </div> <!-- end .testimony-layout-1  -->

                    </div> <!--  end col-md-10  -->

                    <div class="col-md-10 col-md-offset-1 col-sm-12">

                        <div class="testimony-layout-1">
                            <h3 class="people-quote">Recipient Opinion</h3>
                              <img src="frontEnd/images/team_9.jpg" alt="process" class="img-thumbnail center" style="display: block; border-radius: 50%; margin-left: auto; margin-right: auto; width: 20%;" />
                            <p class="testimony-text">
                                <i class="fa fa-quote-left" aria-hidden="true"></i>
                                I wish I could tell you my donor how grateful I am for your selfless act.You gave me new life. We may be coworkers or schoolmates or just two in the same community.I'm very grateful to you.
                                <i class="fa fa-quote-right" aria-hidden="true"></i>
                            </p>

                            <h6>Name</h6>
                            <span>Working Field</span>

                        </div> <!-- end .testimony-layout-1  -->

                    </div> <!--  end col-md-10  -->

                </div>  <!--  end .row  -->

            </div> <!-- end .container  -->

        </section> <!--  end .section-client-testimonial -->

        <!-- SECTION TEAM  -->



        <!-- SECTION CTA -->

        <section class="section-content-block cta-section-3">
            <div class="container wow fadeIn animated">
                <div class="row">
                    <div class="col-md-12">
                        <div class="cta-content text-center">
                            <h2>Join with us and save life</h2>
                            <a class="btn-cta-3" href="#donation">Become Volunteer</a>
                        </div>
                    </div> <!-- end .col-md-12 -->
                </div> <!-- end .row  -->
            </div> <!-- end .container  -->
        </section>   <!-- end cta-section  -->

        <!--  SECTION CAMPAIGNS   -->

        <section class="section-content-block" >

            <div class="container">

                <div class="row section-heading-wrapper">

                    <div class="col-md-12 col-sm-12 text-center">
                        <h2 class="section-heading">Donation Campaigns</h2>
                        <p class="section-subheading">Campaigns to encourage new donors to join and existing to continue to give blood.</p>
                    </div> <!-- end .col-sm-12  -->

                </div> <!-- end .row  -->


                <div class="row">
                  @foreach($sliders as $slider)
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="event-latest">
                            <div class="row">

                                <div class="col-lg-5 col-md-5 hidden-sm hidden-xs">
                                    <div class="event-latest-thumbnail">
                                        <a href="#">
                                            <img src="{{asset('images/sliders')}}/{{$slider->image}}" alt="">
                                        </a>
                                    </div>
                                </div> <!--  col-sm-5  -->

                                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                                    <div class="event-details">
                                        <a class="latest-date" href="#">{{$slider->date}}</a>
                                        <h4 class="event-latest-title">
                                            <a href="{{route('event.profile', $slider->id)  }}">{{$slider->name}}</a>
                                        </h4>
                                        <p>{!!  substr(strip_tags($slider->description), 0, 70) !!}</p>
                                        <div class="event-latest-details">
                                            <a class="author" href="#"><i class="fa fa-clock-o" aria-hidden="true"></i> {{$slider->time}}</a>
                                            <a class="comments" href="#"> <i class="fa fa-map-marker" aria-hidden="true"></i> {{$slider->venue}}</a>
                                        </div>
                                    </div>
                                </div> <!--  col-sm-7  -->

                            </div>

                        </div>
                    </div> <!--  col-sm-6  -->
                    @endforeach
                </div> <!--  end .row  -->


                <div class="row">
                    <div class="col-sm-12 col-md-4 col-md-offset-4 text-center">
                        <a class="btn btn-load-more" href="/events">Load All Campaigns</a>
                    </div>
                </div>

            </div> <!--  end .container  -->

        </section>





        <!-- SECTION LOGO   -->

        <section class="section-client-logo">

            <div class="container wow fadeInUp">

                <div class="row section-heading-wrapper">

                    <div class="col-md-12 col-sm-12 text-center">
                        <h2 class="section-heading">Our Partners</h2>
                        <p class="section-subheading">The Partners who give their valuable amount to fulfill our mission.</p>
                    </div> <!-- end .col-sm-10  -->

                </div> <!-- end .row  -->


                <div class="row">

                    <div class="logo-items logo-layout-1 text-center">
                      @foreach($partners as $partner)

                        <div class="client-logo">

                            <img src="{{asset('images/partners')}}/{{$partner->image}}" alt="" />

                        </div>
                        @endforeach


                    </div> <!-- end .logo-items  -->

                </div> <!-- end row  -->

            </div> <!-- end .container  -->

        </section> <!--  end .section-client-logo -->

        <!-- SECTION BLOG   -->


        <script type="text/javascript">

             $(document).ready(function()
             {
               $('select[name="bname"]').on('change',function(){
                 var bloodgroup_id=jQuery(this).val();
                 if(bloodgroup_id){
                   $.ajax({
                     url:'/getFields1/' +bloodgroup_id,
                     type:"get",
                     dataType:"json",
                     success:function(data){
                       console.log(data);
                       $('select[name="places"]').empty();
                       $.each(data,function(key,value){
                         $('select[name="places"]').append('<option value="'+ key +'">'+ value +'</option>');
                       });
                     }
                   });
                 }
                 else {
                   $('select[name="places"]').empty();
                 }
               });
             });
             </script>
             @jquery
             @toastr_js
             @toastr_render
        @endsection
