@extends('frontEnd.layout.master')
@section('body')
</br></br>
<div class="col-md-12 col-sm-12 text-center">
    <h2 class="section-heading">Blood Donors</h2>
    <p class="section-subheading">Results generated randomly please refresh the page to see new data..</p>
</div> <!-- end .col-sm-10  -->
<table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">#</th>
      <th scope="col">Name</th>
      <th scope="col">Bloodgroup</th>
      <th scope="col">Phone</th>
      <th scope="col">Location</th>

    </tr>
  </thead>
  <tbody>

    @foreach($info as $dt)
    <tr>
      <th scope="row"></th>

      <td>{{$dt->name}}</td>
      <td>

        @foreach($plasmagroups as $bg)
        @if($bg->id==$dt->plasmaplace->plasmabloodgroup_id)
        {{ $bg->name }}
        @endif
        @endforeach
      </td>
      <td>{{$dt->phone}}</td>
      <td>{{$dt->location}}</td>
      <!-- <td>{{$dt->plasmaplace->name}}</td> -->


    </tr>

@endforeach
  </tbody>

</table>
{{$info->links()}}

@endsection
