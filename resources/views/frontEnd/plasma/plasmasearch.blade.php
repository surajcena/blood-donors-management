@extends('frontEnd.layout.master')
<style>
.btn {
	background-color:#2A265F;
	border-radius:28px;
	border:1px solid #18ab29;
	display:inline-block;
	cursor:pointer;
	color:#ffffff;
	font-family:Arial;
	font-size:17px;
	padding:16px 31px;
	text-decoration:none;
	text-shadow:0px 1px 0px #2f6627;
}
.btn:hover {
	background-color:#2dabf9;
}
.btn:active {
	position:relative;
	top:1px;
}
</style>
@section('body')

<script src="{{ asset('jquery.min.js') }}"></script>
</br></br>
<div class="col-md-12 col-sm-12 text-center">
		<h2 class="section-heading">Blood Donors</h2>
		<p class="section-subheading">Search Blood Along With Required Place</p>
</div> <!-- end .col-sm-10  -->
 </br></br>
   <div class="panel-body text-center">
     <div class="col-md-5">
       <select class="form-control" name="plasmagroup" id="plasmagroup">
         <option selected="false">
           Blood Group
         </option>
         @foreach($plasmagroup as $bg)
          <option value="{{$bg->id}}">{{$bg->name}}</option>
         @endforeach
       </select>
     </div>
     <div class="col-md-5">
       <select class="form-control" name="locations" id="locations">
         <option selected="false">
           District...
         </option>
       </select>
     </div>
   </br>
        <div class="col-md-2">
       <button class="btn" type="submit" id="search" name="search">Search <i class="fa fa-search"></i></button>
     </div>
   </div>
   <div>
	 </br>
 </br>



<!-- js part -->
<script type="text/javascript">

     $(document).ready(function()
     {
       $('select[name="plasmagroup"]').on('change',function(){
         var plasmagroup_id=jQuery(this).val();
         if(plasmagroup_id){
           $.ajax({
             url:'/getPlace/' +plasmagroup_id,
             type:"get",
             dataType:"json",
             success:function(data){
               $('select[name="locations"]').empty();
               $.each(data,function(key,value){
                 $('select[name="locations"]').append('<option value="'+ key +'">'+ value +'</option>');
               });
             }
           });
         }
         else {
           $('select[name="locations"]').empty();
         }
       });
     });
     </script>
     <script type="text/javascript">
     $("#search").on("click",function(){
       var link=document.getElementById("locations").value;
       $.ajax({
         url:window.location.href="getInfo/"+link
       });
     });
     </script>
@endsection
