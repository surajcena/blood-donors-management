@extends('frontEnd.layout.master')
@section('body')
@toastr_css
<script src="{{ asset('jquery.min.js') }}"></script>
        <section class="section-appointment" id="donation">

            <div class="container wow fadeInUp">

                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                        <div class="appointment-form-wrapper text-center clearfix">
                            <h3 class="join-heading">I want to be a plasma donor</h3>
                            <form class="appoinment-form" action="/plasmaapplyForm" method="post" enctype="multipart/form-data">
                              {{ csrf_field() }}
                              <div class="form-group col-md-6">
                                  <div class="select-style">
                                      <select class="form-control" name="bname" id="bname">
                                        <option selected="false">
                                          Blood Group
                                        </option>
                                        @foreach($plasmagroup as $bg)
                                          <option value="{{$bg->id}}">{{$bg->name}}
                                          </option>
                                          @endforeach
                                      </select>
                                  </div>
                              </div>
                              <div class="form-group col-md-6">
                                  <div class="select-style">
                                      <select class="form-control" name="places" id="places">
                                          @foreach($place as $pe)
                                          <option value="{{$pe->id}}" selected="false" >District...
                                          </option>
                                          @endforeach

                                      </select>
                                  </div>
                              </div>
                                <div class="form-group col-md-6">
                                    <input id="yname" name="yname" class="form-control" placeholder="Name" type="text">
                                </div>
                                <div class="form-group col-md-6">
                                    <input id="phone" name="phone" class="form-control" placeholder="Phone" type="text">
                                </div>
                                <div class="form-group col-md-6">
                                    <input id="email" name="email" class="form-control" placeholder="Email" type="text">
                                </div>
                                <div class="form-group col-md-6">
                                    <input id="location" name="location" class="form-control" placeholder="Current Location" type="text">
                                </div>




                                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                    <button id="btn_submit" class="btn-submit" type="submit">Register</button>
                                </div>

                            </form>

                        </div> <!-- end .appointment-form-wrapper  -->

                    </div> <!--  end .col-lg-6 -->

                </div> <!--  end .row  -->

            </div> <!--  end .container -->

        </section>  <!--  end .appointment-section  -->


<script type="text/javascript">

 $(document).ready(function()
 {
   $('select[name="bname"]').on('change',function(){
     var bloodgroup_id=jQuery(this).val();
     if(bloodgroup_id){
       $.ajax({
         url:'/getLocation/' +bloodgroup_id,
         type:"get",
         dataType:"json",
         success:function(data){
           console.log(data);
           $('select[name="places"]').empty();
           $.each(data,function(key,value){
             $('select[name="places"]').append('<option value="'+ key +'">'+ value +'</option>');
           });
         }
       });
     }
     else {
       $('select[name="places"]').empty();
     }
   });
 });
 </script>
 @jquery
 @toastr_js
 @toastr_render
@endsection
