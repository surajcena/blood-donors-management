@extends('frontEnd.layout.master')
@section('body')
@toastr_css
<section class="page-header" data-stellar-background-ratio="1.2">

    <div class="container">

        <div class="row">

            <div class="col-sm-12 text-center">


                <h3>
                    Contact Us
                </h3>

                <p class="page-breadcrumb">
                    <a href="/">Home</a> / Contact
                </p>


            </div>

        </div> <!-- end .row  -->

    </div> <!-- end .container  -->

</section> <!-- end .page-header  -->

<!--  MAIN CONTENT  -->



<section class="section-content-block section-contact-block">

    <div class="container">

        <div class="row">

            <div class="col-sm-12 wow fadeInLeft">

                <div class="contact-form-block">

                    <h2 class="contact-title" style="text-align:center">Contact us</h2>

                    <form role="form" class="form-horizontal" method="POST" action="{{url('contactform')}}" enctype="multipart/form-data">
                       {{ csrf_field() }}
                        <div class="form-group">
                            <input type="text" class="form-control" id="user_name" name="user_name" placeholder="Name" data-msg="Please Write Your Name" />
                        </div>

                        <div class="form-group">
                            <input type="email" class="form-control" id="user_email" name="user_email" placeholder="Email" data-msg="Please Write Your Valid Email" />
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" id="user_subject" name="user_subject" placeholder="Subject" data-msg="Please Write Your Message Subject" />
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" id="user_phone" name="user_phone" placeholder="Phone Number" data-msg="Please Write Your Valid Phone Number" />
                        </div>

                        <div class="form-group">
                            <textarea class="form-control" rows="5" name="user_desctiption" id="user_desctiption" placeholder="Message" data-msg="Please Write Your Message" ></textarea>
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-custom">Send Now</button>
                        </div>

                    </form>

                </div> <!-- end .contact-form-block  -->

            </div> <!--  end col-sm-6  -->



        </div> <!-- end row  -->

    </div> <!--  end .container -->

</section> <!-- end .section-content-block  -->
@jquery
@toastr_js
@toastr_render
@endsection
