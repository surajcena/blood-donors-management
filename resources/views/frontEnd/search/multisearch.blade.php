@extends('frontEnd.layout.master')
<style>
.btn {
	background-color:#2A265F;
	border-radius:28px;
	border:1px solid #18ab29;
	display:inline-block;
	cursor:pointer;
	color:#ffffff;
	font-family:Arial;
	font-size:17px;
	padding:16px 31px;
	text-decoration:none;
	text-shadow:0px 1px 0px #2f6627;
}
.btn:hover {
	background-color:#2dabf9;
}
.btn:active {
	position:relative;
	top:1px;
}
</style>
@section('body')

<script src="{{ asset('jquery.min.js') }}"></script>
</br></br>
<div class="col-md-12 col-sm-12 text-center">
		<h2 class="section-heading">Blood Donors</h2>
		<p class="section-subheading">Search Blood Along With Required Place</p>
</div> <!-- end .col-sm-10  -->
 </br></br>
   <div class="panel-body text-center">
     <div class="col-md-5">
       <select class="form-control" name="bloodgroup" id="bloodgroup">
         <option selected="false">
           Blood Group
         </option>
         @foreach($bloodgroup as $bg)
          <option value="{{$bg->id}}">{{$bg->name}}</option>
         @endforeach
       </select>
     </div>
     <div class="col-md-5">
       <select class="form-control" name="places" id="places">
         <option selected="false">
           Field..
         </option>
       </select>
     </div>
	 </br>
        <div class="col-md-2">
       <button class="btn" type="submit" id="search" name="search">Search <i class="fa fa-search"></i></button>
     </div>
   </div>
   <div>
	 </br>
 </br>



<!-- js part -->
<script type="text/javascript">

     $(document).ready(function()
     {
       $('select[name="bloodgroup"]').on('change',function(){
         var bloodgroup_id=jQuery(this).val();
         if(bloodgroup_id){
           $.ajax({
             url:'/getFields/' +bloodgroup_id,
             type:"get",
             dataType:"json",
             success:function(data){
               console.log(data);
               $('select[name="places"]').empty();
               $.each(data,function(key,value){
                 $('select[name="places"]').append('<option value="'+ key +'">'+ value +'</option>');
               });
             }
           });
         }
         else {
           $('select[name="places"]').empty();
         }
       });
     });
     </script>
     <script type="text/javascript">
     $("#search").on("click",function(){
       var link=document.getElementById("places").value;
       $.ajax({
         url:window.location.href="getData/"+link
       });
     });
     </script>
@endsection
