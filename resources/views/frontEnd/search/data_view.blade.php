@extends('frontEnd.layout.master')
@section('body')
</br></br>
<div class="col-md-12 col-sm-12 text-center">
    <h2 class="section-heading">Blood Donors</h2>
    <p class="section-subheading">Results generated randomly please refresh the page to see new data..</p>
</div> <!-- end .col-sm-10  -->
<table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">#</th>
      <th scope="col">Name</th>
      <th scope="col">Bloodgroup</th>
      <th scope="col">Phone</th>
      <th scope="col">Location</th>
      <th scope="col">Last Donated</th>
    </tr>
  </thead>
  <tbody>

    @foreach($data as $dt)
    <?php $days=""; ?>
    @if ($a = $dt->application()->latest()->first())
     <?php $createdAt=$a->created_at->format('M j, Y'); ?>
     <?php $today=date("Y/m/d");?>
    <?php  $datetime1=new DateTime($createdAt);?>
     <?php $datetime2=new DateTime($today);?>
    <?php $interval=$datetime1->diff($datetime2);?>
     <?php $days=$interval->format('%a');?>
  @endif

    <tr>
      <th scope="row"></th>

      <td>{{$dt->name}}</td>
      <td>
        @foreach($bloodgroups as $bg)
        @if($bg->id==$dt->place->bloodgroup_id)

        {{$bg->name}}
        @endif

        @endforeach
      </td>
      <td>{{$dt->phone}}</td>
      <td>{{$dt->location}}</td>
      <!-- <td>{{$dt->place->name}}</td> -->
      <td>
        @if ($a = $dt->application()->latest()->first())

         {{$a->created_at->format('M j, Y')}}</br>Before {{$days}} Days

         @endif
      </td>


    </tr>

@endforeach
  </tbody>

</table>
{{$data->links()}}

@endsection
