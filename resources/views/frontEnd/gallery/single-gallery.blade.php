@extends('frontEnd.layout.master')

@section('body')
</br></br>
<div class="col-md-12 col-sm-12 text-center">
    <h2 class="section-heading">{{$gallery->name}}</h2>
    <p class="section-subheading">{{count($gallery->images)}} photos</p>
</div> <!-- end .col-sm-10  -->

    <div class="kingster-page-wrapper" id="kingster-page-wrapper">
        <div class="gdlr-core-page-builder-body">
            <div class="gdlr-core-pbf-wrapper " style="padding: 100px 20px 30px 20px;">
                <div class="gdlr-core-pbf-background-wrap"></div>
                <div class="gdlr-core-pbf-wrapper-content gdlr-core-js ">
                    <div class="gdlr-core-pbf-wrapper-container clearfix gdlr-core-pbf-wrapper-full">
                        {{--<div class="gdlr-core-pbf-element">
                            <div
                                class="gdlr-core-title-item gdlr-core-item-pdb clearfix  gdlr-core-center-align gdlr-core-title-item-caption-bottom gdlr-core-item-pdlr"
                                style="padding-bottom: 60px ;">
                                <div class="gdlr-core-title-item-title-wrap clearfix">
                                    <h3 class="gdlr-core-title-item-title gdlr-core-skin-title "
                                        style="text-transform: none ;">Album Name</h3></div>
                                <span class="gdlr-core-title-item-caption gdlr-core-info-font gdlr-core-skin-caption">4 photos</span>
                            </div>
                        </div>--}}
                        <div class="gdlr-core-pbf-element">
                            <div
                                class="gdlr-core-gallery-item gdlr-core-item-pdb clearfix  gdlr-core-gallery-item-style-grid">
                                <div class="gdlr-core-gallery-item-holder gdlr-core-js-2 clearfix"
                                     data-layout="fitrows">
                                    @foreach($gallery->images as $images)
                                        <div
                                            class="gdlr-core-item-list gdlr-core-gallery-column gdlr-core-column-15 gdlr-core-item-pdlr gdlr-core-item-mgb">
                                            <div class="gdlr-core-gallery-list gdlr-core-media-image">
                                                <a class="gdlr-core-lightgallery gdlr-core-js "
                                                   href="{{url('images/gallery')}}/{{$images->image}}"
                                                   data-lightbox-group="gdlr-core-img-group-1">
                                                    <img src="{{url('images/gallery')}}/{{$images->image}}" width="700"
                                                         height="660" alt="" style="height: 220px;object-fit: cover;"/>
                                                    <span class="gdlr-core-image-overlay ">
                                                        <i class="gdlr-core-image-overlay-icon gdlr-core-size-22 fa fa-search"></i>
                                                    </span>
                                                </a>
                                                <p>{{$images->name}}</p>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

@endsection
