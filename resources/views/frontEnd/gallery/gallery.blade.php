@extends('frontEnd.layout.master')

@section('body')
<section class="page-header">

    <div class="container">

        <div class="row">

            <div class="col-sm-12 text-center">

                <h3>
                    Gallery
                </h3>

                <p class="page-breadcrumb">
                    <a href="#">Home</a> / My Gallery
                </p>


            </div>

        </div> <!-- end .row  -->

    </div> <!-- end .container  -->

</section> <!-- end .page-header  -->
    <div class="kingster-page-wrapper" id="kingster-page-wrapper">
        <div class="gdlr-core-page-builder-body">
            <div class="gdlr-core-pbf-wrapper " style="padding: 100px 20px 30px 20px;">
                <div class="gdlr-core-pbf-background-wrap"></div>
                <div class="gdlr-core-pbf-wrapper-content gdlr-core-js ">
                    <div class="gdlr-core-pbf-wrapper-container clearfix gdlr-core-pbf-wrapper-full">
                        <div class="gdlr-core-pbf-element">
                            <div
                                class="gdlr-core-gallery-item gdlr-core-item-pdb clearfix  gdlr-core-gallery-item-style-grid">
                                <div class="gdlr-core-gallery-item-holder gdlr-core-js-2 clearfix"
                                     data-layout="fitrows">
                                    @foreach($galleries as $gallery)
                                        <div
                                            class="gdlr-core-item-list gdlr-core-gallery-column  gdlr-core-column-15 gdlr-core-item-pdlr gdlr-core-item-mgb">
                                            <div class="gdlr-core-gallery-list gdlr-core-media-image">
                                                <a class="gdlr-core-js "
                                                   href="{{route('gsingle', $gallery->id)}}"
                                                   data-lightbox-group="gdlr-core-img-group-1">
                                                    <img src="{{url('images/gallery')}}/{{$gallery->image}}" width="700"
                                                         height="660" alt="" style="height: 220px;object-fit: cover;"/>
                                                    <span class="gdlr-core-image-overlay ">
                                                        <i class="gdlr-core-image-overlay-icon gdlr-core-size-22 fa fa-search"></i>
                                                    </span>
                                                </a>
                                                <h5>{{$gallery->name}}</h5>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

@endsection
